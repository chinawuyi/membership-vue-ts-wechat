# 环境
NODE_ENV=production

# 接口前缀
VITE_API_BASEPATH=pro

# 打包路径
VITE_BASE_PATH=/

# 是否删除debugger
VITE_DROP_DEBUGGER=false

# 是否删除console.log
VITE_DROP_CONSOLE=false

# 是否sourcemap
VITE_SOURCEMAP=false

# 输出路径
VITE_OUT_DIR=dist-pro

# 标题
VITE_APP_TITLE=会员管理系统
