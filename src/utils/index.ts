import { first, toUpper } from 'lodash-es'
import { useCache } from '@/hooks/web/useCache';

const { wsCache } = useCache()

export function getHotelCode() {
  return toUpper(first(location.hostname.split('.')))
}

export function getOpenIdKey() {
  return `NMM:${getHotelCode()}:openid`
}

export function getOpenId() {
  return process.env.NODE_ENV === 'development' ? 'oZeckxFX_Lp_1WIkXmbCyqJ6YNm4' : wsCache.get(getOpenIdKey())
}