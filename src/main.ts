import { createApp } from 'vue'
import App from './App.vue'
import 'vant/es/toast/style'
// import router from './router'

// 路由
import { setupRouter } from './router'
import router from './router'

// 引入状态管理
import { setupStore } from '@/store'

// 创建实例
const setupAll = async () => {
  const app = createApp(App)

  app.config.errorHandler = (err, instance, info) => {
    // 处理错误，例如：报告给一个服务
    console.log('全局处理错误对象', err)
    console.log('instance', instance)
    console.log('info', info)
  }
  
  setupStore(app)

  setupRouter(app)

  router.isReady().then(() => {
    app.mount('#app')
  })
}

setupAll()