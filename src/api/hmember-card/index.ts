import request from '@/config/axios'
import type { OpenUser } from './types'

export const code2accesstoken = (params: any): Promise<IResponse<OpenUser>> => {
  return request.get({ url: '/wechat/code2accesstoken', params })
}
