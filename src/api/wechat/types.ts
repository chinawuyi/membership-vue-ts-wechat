export type OpenUser = {
  openid: string
  mobile: string
  guest_title: string
  last_name: string
  xlast: string
  token: string
}

export type HMemberType = {
  id: string | undefined
  hotel_id: string | undefined
  code: string | undefined
  name: string | undefined
  card_category: string | undefined
  expired_days: number | undefined
  limited: number | undefined
  allow_update_expired_days: boolean | undefined
  allow_update_limited: boolean | undefined
  day_limited: number | undefined
  reference: string | undefined
  url: string | undefined
  disabled: boolean | undefined
  sequence: number | undefined
  created_id: string | undefined
  updated_id: string | undefined
  created_at: string | Date | undefined
  updated_at: string | Date | undefined
  version: number
}

export type HMemberCard = {
  id: string | undefined
  hotel_id: string | undefined
  name_id: string | undefined
  uid: string | undefined
  member_number: string | undefined
  limit: number | undefined
  begin_date: Date | string | undefined
  end_date:  Date | string | undefined
  card_type: string | undefined
  hmember_type_id: string | undefined
  hmember_type: HMemberType | undefined
  consumption_count: number
  status: string
  status_desc: string
  check_number: string | undefined
  saler_id: string | undefined
  payment_method_id: string | undefined
  amount: number | undefined
  reference: string | undefined
  created_id: string | undefined
  updated_id: string | undefined
  created_at: string | Date | undefined
  updated_at: string | Date | undefined
  version: number
}


export type HMemberCardTransaction = {
  id: string | undefined
  hotel_id: string | undefined
  hmember_card_id: string | undefined
  member_number: string | undefined
  hmember_type_id: string | undefined
  transaction_type: string | undefined
  room: string | undefined
  guest_name: string | undefined
  mobile: string | undefined
  check_number: string | undefined
  amount: number | undefined
  locker: string | undefined
  checkin_time: Date | string | undefined
  checkout_time: Date | string | undefined
  consumption: number | undefined
  balance: number | undefined
  reference: string | undefined
  created_id: string | undefined
  updated_id: string | undefined
  created_at: string | Date | undefined
  updated_at: string | Date | undefined
  version: number
}

export type HotelInfo = {
  hotel_name: string
  images: string[],
  my_image: string
}