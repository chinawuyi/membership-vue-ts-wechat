import request from '@/config/axios'
import type { OpenUser, HMemberCard, HMemberCardTransaction, HotelInfo } from './types'

export const code2accesstokenApi = (params: any): Promise<IResponse<OpenUser>> => {
  return request.get({ url: '/wechat/code2accesstoken', params })
}

export const openid2tokenApi = (params: any): Promise<IResponse<OpenUser>> => {
  return request.get({ url: '/wechat/openid2token', params })
}

export const getCaptchaApi = (data: any): Promise<IResponse<OpenUser>> => {
  return request.post({ url: '/wechat/sms/captcha', data })
}

export const captchaLoginApi = (data: any): Promise<IResponse<OpenUser>> => {
  return request.post({ url: '/wechat/captcha/login', data })
}

export const getHMemberCardListApi = (): Promise<IResponse<HMemberCard[]>> => {
  return request.get({ url: '/wechat/hmember-cards' })
}

export const getHMemberCardHistoryApi = (params: any): Promise<IResponse<IResData<HMemberCardTransaction>>> => {
  return request.get({ url: '/wechat/hmember-cards-history', params })
}

export const getHMemberCardAllApi = (params: any): Promise<IResponse<IResData<HMemberCard>>> => {
  return request.get({ url: '/wechat/hmember-cards/all', params })
}

export const getHotelInfoApi = (): Promise<IResponse<HotelInfo>> => {
  return request.get({ url: '/wechat/hotel-info' })
}
