import { createRouter, createWebHistory } from 'vue-router'
import type { App } from 'vue'

const routes = [
  {
    name: 'notFound',
    path: '/:path(.*)+',
    redirect: {
      name: 'cards'
    }
  },
  {
    name: 'user',
    path: '/user',
    component: () => import('@/view/user/index.vue'),
    meta: {
      title: '会员中心'
    }
  },
  {
    name: 'cards',
    path: '/cards',
    component: () => import('@/view/cards/index.vue'),
    meta: {
      title: '健身中心'
    }
  },
  {
    name: 'history',
    path: '/history',
    component: () => import('@/view/history/index.vue'),
    meta: {
      title: '健身历史'
    }
  },
  {
    name: 'cardHistory',
    path: '/card-history',
    component: () => import('@/view/card-history/index.vue'),
    meta: {
      title: '购卡历史'
    }
  },
]

const router = createRouter({
  routes,
  history: createWebHistory()
})

router.beforeEach((to, from, next) => {
  console.log(from)
  const title = to?.meta?.title
  if (title) {
    document.title = title as string
  }
  next()
})

export const setupRouter = (app: App<Element>) => {
  app.use(router)
}

export default router
